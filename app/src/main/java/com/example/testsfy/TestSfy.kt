package com.example.testsfy

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestSfy:Application()