package com.example.testsfy.ui.viewmodels

import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testsfy.data.DataStoreRepository
import com.example.testsfy.ui.view.OnBoardingActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val repository: DataStoreRepository
) : ViewModel() {

    private val FIELD_KEY = "isLogged"

    fun isFirstTime() : Boolean? = runBlocking {
        repository.getBoolean(FIELD_KEY)
    }

    fun setFirstTimeFalse(){
        viewModelScope.launch {
            repository.putBoolean(FIELD_KEY, true)
        }
    }
}