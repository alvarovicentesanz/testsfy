package com.example.testsfy.ui.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.testsfy.ui.viewmodels.SplashViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private val viewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            TimeUnit.MILLISECONDS.sleep(2000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        checkFirstInit()
    }

    fun checkFirstInit() {

        if (viewModel.isFirstTime() == null ) {
            viewModel.setFirstTimeFalse()
            startActivity(Intent(this, OnBoardingActivity::class.java))
        } else {
            startActivity(Intent(this, MainActivity::class.java))
        }

    }

}