package com.example.testsfy.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testsfy.databinding.ActivityMainBinding
import com.example.testsfy.model.ItemData
import com.example.testsfy.ui.RecyclerViewAdapter
import com.example.testsfy.ui.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity @Inject constructor() : AppCompatActivity(), RecyclerViewAdapter.onItemClick {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var list: List<ItemData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpUiElements()

        viewModel.getData()
    }

    fun setUpUiElements(){
        viewModel.loaded.observe(this){
            if(it){
                list = viewModel.loadList()
                binding.loading.visibility = View.GONE
                binding.recyclerView.apply {
                    layoutManager = LinearLayoutManager(this@MainActivity)
                }

                binding.recyclerView.adapter = RecyclerViewAdapter(list, this)
            }else binding.loading.visibility = View.VISIBLE
        }

        viewModel.error.observe(this){
            if(it){
                binding.loading.visibility = View.GONE
                Toast.makeText(this, "Error en la descarga de imagenes", Toast.LENGTH_SHORT).show()
                binding.retryBtn.visibility = View.VISIBLE
            }
        }

        binding.retryBtn.setOnClickListener {
            binding.retryBtn.visibility = View.GONE
            viewModel.getData()
        }
    }

    override fun onItemSelected(item: ItemData) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("image", item.image_url)
        startActivity(intent)
    }

    override fun onBackPressed() {return}



}