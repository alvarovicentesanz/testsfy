package com.example.testsfy.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testsfy.dom.GetFlowerData
import com.example.testsfy.dom.LoadDataList
import com.example.testsfy.model.ItemData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val loadDataList: LoadDataList,
    private val getFlowerData: GetFlowerData
) : ViewModel() {
    val error = MutableLiveData<Boolean>()
    val loaded = MutableLiveData<Boolean>()

    fun getData(){

        viewModelScope.launch {
            loaded.postValue(false)
            var res = getFlowerData()
            if (res != null) {
                loaded.postValue(true)
            }else{
                error.postValue(true)
            }
        }
    }

    fun loadList() :List<ItemData> = loadDataList()

}