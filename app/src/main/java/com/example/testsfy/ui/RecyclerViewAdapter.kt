package com.example.testsfy.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testsfy.R
import com.example.testsfy.model.ItemData

class RecyclerViewAdapter (var items: List<ItemData>, val listener: onItemClick) : RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>(){

    interface onItemClick{
       fun onItemSelected(item:ItemData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = layoutInflater.inflate(R.layout.item_layout, parent, false)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem =  items[position]

        holder.des.text = currentItem.description ?: "Sin descripción"

        Glide.with(holder.img)
            .load(currentItem.image_url)
            .circleCrop()
            .placeholder(R.drawable.loading)
            .into(holder.img)

        holder.img.setOnClickListener {
            listener.onItemSelected(currentItem)
        }
    }

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        var img : ImageView
        var des : TextView

        init {
            img = itemView.findViewById(R.id.flower_img)
            des = itemView.findViewById(R.id.description)
        }
    }

}