package com.example.testsfy.model

class ItemData (val id: String? = null, val description: String? = null, val image_url: String? = null)