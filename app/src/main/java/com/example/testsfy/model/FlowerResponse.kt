package com.example.example

import com.google.gson.annotations.SerializedName


data class FlowerResponse (

  @SerializedName("total"       ) var total      : Int?               = null,
  @SerializedName("total_pages" ) var totalPages : Int?               = null,
  @SerializedName("results"     ) var results    : ArrayList<Result> = arrayListOf()

)