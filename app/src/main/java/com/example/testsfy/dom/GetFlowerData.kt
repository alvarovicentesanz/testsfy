package com.example.testsfy.dom

import com.example.example.FlowerResponse
import com.example.testsfy.data.Repository
import javax.inject.Inject

class GetFlowerData @Inject constructor(private val repository: Repository){

    suspend operator fun invoke():FlowerResponse?{
        return repository.getFlowersData()
    }
}