package com.example.testsfy.dom

import com.example.testsfy.data.Repository
import com.example.testsfy.model.ItemData
import javax.inject.Inject

class LoadDataList @Inject constructor(private val repository: Repository){
    operator fun invoke() : List<ItemData> = repository.loadList()

}