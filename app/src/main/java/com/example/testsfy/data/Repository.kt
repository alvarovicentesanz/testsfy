package com.example.testsfy.data

import com.example.example.FlowerResponse
import com.example.testsfy.model.ItemData
import javax.inject.Inject
import kotlin.random.Random

class Repository @Inject constructor(
    private val api: APIService,
    private val flowerDataProvider: FlowerDataProvider,
    private val prefs: DataStoreRepository
) {

    private val FIELD_KEY = "numPage"

    suspend fun getFlowersData(): FlowerResponse? {

        val numPage = prefs.getInt(FIELD_KEY) ?: 1
        val newPage = if (numPage > 998) 1 else numPage + 1
        prefs.putInt(FIELD_KEY, newPage)

        val res = api.getFlowers(numPage.toString())
        if (res != null) {
            flowerDataProvider.data = res
            flowerDataProvider.result = res.results
        }

        return res
    }

    fun loadList(): List<ItemData> {
        var dataList = mutableListOf<ItemData>()
        flowerDataProvider.result.forEach {
            dataList.add(ItemData(it.id, it.altDescription, it.urls?.regular))
        }
        return dataList.toList()
    }

}