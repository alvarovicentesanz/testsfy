package com.example.testsfy.data

import com.example.example.FlowerResponse
import com.example.testsfy.BuildConfig
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIMethods {
    @GET("/search/photos?&query=flower&client_id=${BuildConfig.API_KEY}")
    suspend fun getFlowers(@Query("page") page : String): Response<FlowerResponse?>?
}