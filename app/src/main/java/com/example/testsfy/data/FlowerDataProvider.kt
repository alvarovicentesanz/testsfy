package com.example.testsfy.data

import com.example.example.FlowerResponse
import com.example.example.Result
import com.example.testsfy.model.ItemData
import javax.inject.Inject
import javax.inject.Singleton
@Singleton
class FlowerDataProvider @Inject constructor() {

    var data: FlowerResponse? = null
    var result: List<Result> = emptyList()

}