package com.example.testsfy.data

import com.example.example.FlowerResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class APIService @Inject constructor(private val api: APIMethods) {

    suspend fun getFlowers(page: String): FlowerResponse? {
        return withContext(Dispatchers.IO) {
            var res : Response<FlowerResponse?>? = null
            try {
                res = api.getFlowers(page)!!
            } catch (e: Exception) {
                e.printStackTrace()
            }
            res?.body() ?: null
        }
    }
}